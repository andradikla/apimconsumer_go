package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"
)

var clientID = ""
var clientSecret = ""
var accessToken = ""
var accessCode = ""

const tokenURL = "https://localhost:8243/token"
const authURL = "https://localhost:8243/authorize"
const redirectURL = "http://localhost:3000/callback"

var loggedIn = false

func main() {

	fs := http.FileServer(http.Dir("html"))
	http.Handle("/", fs)

	http.HandleFunc("/login", loginM)

	http.HandleFunc("/menu", getMenu)

	http.HandleFunc("/logout", logoutM)

	http.HandleFunc("/callback", callback)

	http.ListenAndServe(":3000", nil)
}

//OAuthAccessResponse response oauth access token
type OAuthAccessResponse struct {
	AccessToken string `json:"access_token"`
}

//MenuItem indicates pizza item
type MenuItem struct {
	Name        string
	Description string
	Price       string
	Icon        string
}

//PizzaMenuData for pizza menu
type PizzaMenuData struct {
	Title     string
	MenuItems []MenuItem
}

//LogInDetails to get client id, secret
type LogInDetails struct {
	ClientID     string
	ClientSecret string
}

func loginM(w http.ResponseWriter, r *http.Request) {

	if !loggedIn {
		fmt.Println("handle login")

		loginDetails := LogInDetails{
			ClientID:     r.FormValue("client_id"),
			ClientSecret: r.FormValue("client_secret"),
		}

		fmt.Print("login details : ")
		fmt.Println(loginDetails)

		clientID = loginDetails.ClientID
		clientSecret = loginDetails.ClientSecret

		authReqURL := fmt.Sprintf("%s?client_id=%s&redirect_uri=%s&response_type=code", authURL, loginDetails.ClientID, redirectURL)
		fmt.Println(authReqURL)

		// authReq, authErr := http.NewRequest(http.MethodGet, authReqURL, nil)
		// if authErr != nil {
		// 	fmt.Fprintf(os.Stdout, "could not create HTTP request: %v", authErr)
		// 	w.WriteHeader(http.StatusBadRequest)
		// }

		// resq, _ := http.Get(authReqURL)
		// fmt.Println("get response  >>>>>>>>>> ")
		// fmt.Println(resq)
		w.Header().Set("Location", authReqURL)
		w.WriteHeader(http.StatusFound)
	} else {
		w.Header().Set("Location", "/welcome.html")
		w.WriteHeader(http.StatusFound)
	}
}

func callback(w http.ResponseWriter, r *http.Request) {

	httpClient := http.Client{}

	fmt.Println("within callback")
	err := r.ParseForm()
	if err != nil {
		fmt.Fprintf(os.Stdout, "could not parse query: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	code := r.FormValue("code")
	fmt.Print("access code : ")
	fmt.Println(code)

	form := url.Values{
		"client_id":     {clientID},
		"client_secret": {clientSecret},
		"code":          {code},
		"grant_type":    {"authorization_code"},
		"redirect_uri":  {"http://localhost:3000/callback"}}

	req, err := http.NewRequest(http.MethodPost, tokenURL, strings.NewReader(form.Encode()))
	if err != nil {
		fmt.Fprintf(os.Stdout, "could not create HTTP request: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	//req.Header.Set("accept", "application/json")

	res, err := httpClient.Do(req)
	if err != nil {
		fmt.Fprintf(os.Stdout, "could not send HTTP request: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer res.Body.Close()

	var t OAuthAccessResponse
	if err := json.NewDecoder(res.Body).Decode(&t); err != nil {
		fmt.Fprintf(os.Stdout, "could not parse JSON response: %v", err)
		w.WriteHeader(http.StatusBadRequest)
	}

	accessToken = t.AccessToken
	fmt.Print("access token : ")
	fmt.Println(accessToken)

	loggedIn = true

	w.Header().Set("Location", "/welcome.html")
	w.WriteHeader(http.StatusFound)
}

func getMenu(w http.ResponseWriter, r *http.Request) {

	httpClient1 := http.Client{}
	if loggedIn {
		fmt.Println("handling menu items")
		fmt.Println(accessToken)

		//get menu from pizza shack API
		req, err := http.NewRequest(http.MethodGet, "https://localhost:8243/pizzashack/1.0.0/menu", nil)
		if err != nil {
			fmt.Fprintf(os.Stdout, "could not create HTTP request: %v", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		req.Header.Set("accept", "application/json")

		req.Header.Set("Authorization", "Bearer "+accessToken)

		res1, err1 := httpClient1.Do(req)
		if err1 != nil {
			fmt.Fprintf(os.Stdout, "could not send HTTP request: %v", err1)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		data, _ := ioutil.ReadAll(res1.Body)

		tmpl := template.Must(template.ParseFiles("menu.html"))
		PizzaData := listMenuItems(data)

		tmpl.Execute(w, PizzaData)
		w.Header().Set("Location", "/menu.html")
		w.WriteHeader(http.StatusFound)
	} else {
		fmt.Println("user is not logged in. Please login")
		w.Header().Set("Location", "/index.html")
		w.WriteHeader(http.StatusFound)
	}

}

func listMenuItems(pizzaStr []byte) PizzaMenuData {

	var items []MenuItem
	json.Unmarshal(pizzaStr, &items)
	fmt.Println(items)

	Data := PizzaMenuData{
		Title:     "pizza menu",
		MenuItems: items,
	}
	return Data
}

func logoutM(w http.ResponseWriter, r *http.Request) {
	loggedIn = false
	accessToken = ""
	fmt.Println("user is not logged in. Please login")
	w.Header().Set("Location", "/index.html")
	w.WriteHeader(http.StatusFound)
}
